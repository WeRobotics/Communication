#include "system.h"

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>

wer::com::System::System(std::shared_ptr<TimeoutHandler> timeoutHandler, uint8_t systemId, SystemType systemType) :
_timeoutHandler(timeoutHandler),
_systemId(systemId),
_systemType(systemType),
_canOperate(true),
_validConnections(0)
{

}

wer::com::System::~System() {
	for(auto connection : _connections) {
		_timeoutHandler->remove(connection.second.cookie);
	}
}

void wer::com::System::setEventCallback(std::function<void(uint8_t, SystemEvent)> eventCallback) {
	_eventCallback = eventCallback;
}

wer::com::SystemType wer::com::System::getType() {
	return _systemType;
}

bool wer::com::System::canOperate() {
	return _canOperate;
}

void wer::com::System::setOperate(bool canOperate) {
	_canOperate = canOperate;
}

void wer::com::System::heartbeatTimeout(uint8_t interface) {
	auto connection = _connections.find(interface);
	if(connection != _connections.end()) {
		connection->second.connected = false;

		_validConnections--;

		if(_eventCallback) {
			_eventCallback(interface, SystemEvent::DISCONNECTION);
		}
		
		if(_validConnections) {
			LogInfo() << "System " << unsigned(_systemId) << " on interface " << unsigned(interface) << " disconnected.";

		}
		else {
			LogInfo() << "System " << unsigned(_systemId) << " on interface " << unsigned(interface) << " disconnected. Connection lost.";
		}
	}
}

void wer::com::System::heartbeat(uint8_t interface, uint16_t address) {
	auto connection = _connections.find(interface);
	if(connection != _connections.end()) {
		if(connection->second.connected) {
			_timeoutHandler->refresh(connection->second.cookie);
			if(connection->second.address != address) {
				connection->second.address = address;
				LogInfo() << "System " << unsigned(_systemId) << " on interface " << unsigned(interface) << " changed address to " << unsigned(address) << ".";
			}
		}
		else {
			connection->second.connected = true;
			connection->second.address = address;

			_timeoutHandler->add( std::bind(&wer::com::System::heartbeatTimeout, this, interface), 3.0f, &(connection->second.cookie));

			_validConnections++;

			if(_eventCallback) {
				_eventCallback(interface, SystemEvent::RECONNECTION);
			}

			LogInfo() << "System " << unsigned(_systemId) << " on interface " << unsigned(interface) << " reconnected with address " << unsigned(address) << ".";
		}
	}
	else {
		ConnectionInfo newConnection;
		newConnection.address = address;
		newConnection.connected = true;
		_timeoutHandler->add( std::bind(&wer::com::System::heartbeatTimeout, this, interface), 3.0f, &(newConnection.cookie));

		_connections[interface] = newConnection;

		if(_eventCallback) {
			_eventCallback(interface, SystemEvent::CONNECTION);
		}

		_validConnections++;
		LogInfo() << "System " << unsigned(_systemId) << " on interface " << unsigned(interface) << " connected with address " << unsigned(address) << ".";
	}
}

bool wer::com::System::connected(uint8_t interface) {
	auto connection = _connections.find(interface);
	if(connection != _connections.end()) {
		return connection->second.connected;
	}
	else {
		return false;
	}
}

bool wer::com::System::connected() {
	return _validConnections > 0;
}

uint16_t wer::com::System::getAddress(uint8_t interface) {
	auto connection = _connections.find(interface);
	if(connection != _connections.end()) {
		return connection->second.address;
	}
	else {
		return 0;
	}
}