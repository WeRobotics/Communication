#include "modem_logger.h"
#include "Library/logging/log.h"


#include <string.h>

wer::com::ModemLogger::ModemLogger(std::shared_ptr<wer::hal::Connection> connection, std::shared_ptr<TimeoutHandler> timeoutHandler, std::shared_ptr<CallEveryHandler> timerHandler) :
_connection(connection),
_timeoutHandler(timeoutHandler),
_timerHandler(timerHandler),
_cmdIndex(0)
{
	resetReception();
	connection->registerCallback(std::bind(&ModemLogger::decodeResponse, this, std::placeholders::_1, std::placeholders::_2));
}

wer::com::ModemLogger::~ModemLogger() {
	stop();
}

void wer::com::ModemLogger::start() {
	_timerHandler->add(std::bind(&ModemLogger::sendNextAtCommand, this), 0.6f, &_sendAtCommandCookie);
}

void wer::com::ModemLogger::stop() {
	_timerHandler->remove(_sendAtCommandCookie);
}

wer::com::ModemLogger::CopsResponse wer::com::ModemLogger::getCops() {
	std::lock_guard<std::mutex> lock(_answerMutex);
	return _actualCops;
}

wer::com::ModemLogger::CsqResponse wer::com::ModemLogger::getCsq() {
	std::lock_guard<std::mutex> lock(_answerMutex);
	return _actualCsq;
}

wer::com::ModemLogger::BandResponse wer::com::ModemLogger::getBand() {
	std::lock_guard<std::mutex> lock(_answerMutex);
	return _actualBand;
}

void wer::com::ModemLogger::sendNextAtCommand() {

	static const char* atCommands[] = {"AT+COPS?\r\n", "AT+CSQ\r\n", "AT#BND?\r\n", "AT#MONI\r\n", "AT#SERVINFO\r\n"};

	if(_cmdIndex >= 0 && _cmdIndex < 5) {
		_connection->sendMessage(reinterpret_cast<const uint8_t*>(atCommands[_cmdIndex]), strlen(atCommands[_cmdIndex]));
	}

	_cmdIndex = (_cmdIndex + 1) % 5;
}

void wer::com::ModemLogger::resetReception() {
	_isAnswered = false;
	_decodeIndex = 0;
	_decodeStep = 0;

	memset(&_tempCops, 0, sizeof(_tempCops));
	memset(&_tempCsq, 0, sizeof(_tempCsq));
	memset(&_tempBand, 0, sizeof(_tempBand));
}

void wer::com::ModemLogger::decodeResponse(uint8_t* data, uint16_t dataLength) {
	for(int i = 0; i < dataLength; i++) {
		if(data[i] == '\n' || data[i] == '\r') {
			if(_isAnswered) {
				if(_decodeCommand == 0) {
					_answerMutex.lock();
					_actualCops = _tempCops;
					_actualCops.time = std::chrono::steady_clock::now();
					_answerMutex.unlock();
					//LogInfo() << "COPS - " << _actualCops.network << ", " << unsigned(_actualCops.mode);
				}
				else if(_decodeCommand == 1) {
					_answerMutex.lock();
					_actualCsq = _tempCsq;
					_actualCsq.time = std::chrono::steady_clock::now();
					_answerMutex.unlock();
					//LogInfo() << "CSQ - " << unsigned(_actualCsq.ber) << ", " << unsigned(_actualCsq.rssi);
				}
				else if(_decodeCommand == 2) {
					_answerMutex.lock();
					_actualBand = _tempBand;
					_actualBand.time = std::chrono::steady_clock::now();
					_answerMutex.unlock();
					//LogInfo() << "CSQ - " << unsigned(_actualCsq.ber) << ", " << unsigned(_actualCsq.rssi);
				}
				else if(_decodeCommand == 3) {
					LogSensor() << _buffer;
				}
			}

			resetReception();
			continue;
		}

		if(_decodeIndex == 0) {
			// First line char, check if this is a '+'
			if(data[i] == '+' || data[i] == '#') {
				_isAnswered = true;
			}
			else {
				_isAnswered = false;
			}
			_decodeIndex++;
		}
		else if(_isAnswered) {
			// A '+' was received at the beginning of a line.
			if(_decodeStep == 0 && data[i] == ':') {
				if(strcmp(_atRecvBuffer, "COPS") == 0) {
					_decodeCommand = 0;
					_decodeStep++;
				}
				else if(strcmp(_atRecvBuffer, "CSQ") == 0) {
					_decodeCommand = 1;
					_decodeStep++;
				}
				else if(strcmp(_atRecvBuffer, "BND") == 0) {
					_decodeCommand = 2;
					_decodeStep++;
				}
				else if(strcmp(_atRecvBuffer, "MONI") == 0) {
					_decodeCommand = 3;
					_decodeStep++;
					_buffer = "#MONI: ";
				}
				else if(strcmp(_atRecvBuffer, "SERVINFO") == 0) {
					_decodeCommand = 3;
					_decodeStep++;
					_buffer = "#SERVINFO: ";
				}
				else {
					resetReception();
				}
			}
			else if(_decodeStep == 0) {
				if(data[i] >= 'A' && data[i] <= 'Z' && _decodeIndex < RECV_BUF_SIZE) {
					_atRecvBuffer[_decodeIndex-1] = data[i];
					_atRecvBuffer[_decodeIndex] = '\0';
					_decodeIndex++;
				}
				else {
					resetReception();
				}
			}
			else {
				if(_decodeCommand == 3) {
					_buffer += data[i];
				}
				else if(data[i] == ',') {
					_decodeStep++;
				}
				else {
					if(_decodeCommand == 0) {
						decodeCops(data[i], _decodeStep-1);
					}
					else if(_decodeCommand == 1) {
						decodeCsq(data[i], _decodeStep-1);
					}
					else if(_decodeCommand == 2) {
						decodeBand(data[i], _decodeStep-1);
					}
					else {
						resetReception();
					}	
				}
			}
		}
		else {
			continue;
		}
	}
}

void wer::com::ModemLogger::decodeCsq(uint8_t data, uint8_t step) {
	switch (step)
	{
	case 0:
		if(data >= '0' && data <= '9') {
			_tempCsq.rssi = _tempCsq.rssi * 10 + data - '0';
		}
		else if (data != ' ') {
			resetReception();
		}
		break;
	
	case 1:
		if(data >= '0' && data <= '9') {
			_tempCsq.ber = _tempCsq.ber * 10 + data - '0';
			_tempCsq.valid = true;
		}
		else {
			resetReception();
		}
		break;

	default:
		resetReception();
		break;
	}

}

void wer::com::ModemLogger::decodeBand(uint8_t data, uint8_t step) {
	switch (step)
	{
	case 0:
		if(data >= '0' && data <= '9') {
			_tempBand.gsm = _tempBand.gsm * 10 + data - '0';
		}
		else if (data != ' ') {
			resetReception();
		}
		break;
	
	case 1:
		if(data >= '0' && data <= '9') {
			_tempBand.hspa = _tempBand.hspa * 10 + data - '0';
		}
		else if (data != ' ') {
			resetReception();
		}
		break;

	case 2:
		if(data >= '0' && data <= '9') {
			_tempBand.lte = _tempBand.lte * 10 + data - '0';
		}
		else if (data != ' ') {
			resetReception();
		}
		break;

	default:
		resetReception();
		break;
	}

}

void wer::com::ModemLogger::decodeCops(uint8_t data, uint8_t step) {
	//+COPS: <mode>[, <format>, <oper>,< AcT>] 
	switch (step)
	{
	case 0:
		if(data >= '0' && data <= '9') {
			_tempCops.mode = _tempCops.mode * 10 + data - '0';
		}
		else if (data != ' ') {
			resetReception();
		}
		break;
	
	case 1:
		if(data >= '0' && data <= '9') {
			_tempCops.format = _tempCops.format * 10 + data - '0';
		}
		else {
			resetReception();
		}
		break;

	case 2:
		if(data != '"') {
			if(_tempCops.networkStrLength + 1 < 30) {
				_tempCops.network[_tempCops.networkStrLength] = data;
				_tempCops.network[_tempCops.networkStrLength + 1] = '\0';
				_tempCops.networkStrLength++;
			}
		}
		
		break;

	case 3:
		if(data >= '0' && data <= '9') {
			_tempCops.act = _tempCops.act * 10 + data - '0';
			_tempCops.valid = true;
		}
		else {
			resetReception();
		}
		break;
	
	default:
		resetReception();
		break;
	}
}