#include "digi_radio_api.h"

#include <string.h>

#include <iomanip>

#define DIGI_BROADCAST_ADDR			0x000000000000FFFF

#define DIGI_START_DELIMITER		0x7E
#define DIGI_START_BYTE_LENGTH		0x01	//MSB first
#define DIGI_START_BYTE_FRAME		0x03

namespace wer {
    namespace com {

		enum class FrameId {
			AT_CMD			= 0x08,
			AT_RESPONSE		= 0x88,

			RT_AT_CMD		= 0x17,
			RT_AT_ACK		= 0x97,

			MODEM_STATUS	= 0x8A,
			ROUTE_INFO		= 0x8D,

			TX				= 0x10,
			TX_STATUS		= 0x8B,

			RX_LEG			= 0x80,
			RX				= 0x90
		};
/*
		enum class OverheadLength {
			ALL_FRAME			= 4,
			ROUTE_INFO 			= 42,
			RX_RECEIVE 			= 12,
			RX_LEGACY_RECEIVE 	= 12,
			AT_CMD_FRAME		= 4,
			AT_RECEIVE_FRAME	= 5,
			AT_RSSI_CMD			= 2,
			TX_FRAME 			= 14
		};
*/

		constexpr uint8_t ALL_FRAME			= 4;
		constexpr uint8_t ROUTE_INFO 		= 42;
		constexpr uint8_t RX_RECEIVE 		= 12;
		constexpr uint8_t RX_LEGACY_RECEIVE = 12;
		constexpr uint8_t AT_CMD_FRAME		= 4;
		constexpr uint8_t AT_RECEIVE_FRAME	= 5;
		constexpr uint8_t AT_RSSI_CMD		= 2;
		constexpr uint8_t TX_FRAME 			= 14;
/*
		OverheadLength operator+(OverheadLength lhs, OverheadLength rhs) {
		    return static_cast<OverheadLength> (
		        static_cast<std::underlying_type<OverheadLength>::type>(lhs) +
		        static_cast<std::underlying_type<OverheadLength>::type>(rhs)
		    );
		}
*/
		TransmitOption operator|(TransmitOption lhs, TransmitOption rhs) {
		    return static_cast<TransmitOption> (
		        static_cast<std::underlying_type<TransmitOption>::type>(lhs) |
		        static_cast<std::underlying_type<TransmitOption>::type>(rhs)
		    );
		}
/*
		uint16_t operator+( uint16_t lhs, OverheadLength rhs) {
		    return static_cast<std::underlying_type<OverheadLength>::type>(rhs) + lhs;
		}

		uint16_t operator-( uint16_t lhs, OverheadLength rhs) {
		    return static_cast<std::underlying_type<OverheadLength>::type>(rhs) - lhs;
		}
*/

		enum class AtCmdStatus {
			OK					= 0,
			ERROR				= 1,
			INVALID_COMMAND		= 2,
			INVALID_PARAMETER	= 3
		};

		enum class DeliveryStatus {
			SUCCESS				= 0x00,
			MAC_ACK_FAILURE		= 0x01,
			COLLISION_FAILURE	= 0x02,
			NETWORK_ACK_FAILURE	= 0x21,
			ROUTE_NOT_FOUND		= 0x25,
			INTERNAL_RES_ERROR	= 0x31,
			INTERNAL_ERROR 		= 0x32
		};

		enum class DiscoveryStatus {
			NO_DISCOVERY_OVERHEAD	= 0x00,
			ROUTE_DISCOVERY			= 0x01
		};
	}
}

wer::com::DigiRadioApi::DigiRadioApi(std::shared_ptr<wer::hal::Connection> connection, uint8_t priority, std::string name) :
Link(priority, name),
_connection(connection)
{
	_connection->registerCallback(std::bind(&wer::com::DigiRadioApi::processIncomingData, this, std::placeholders::_1, std::placeholders::_2));
}

wer::com::DigiRadioApi::~DigiRadioApi() {

}

uint8_t wer::com::DigiRadioApi::broadcast(uint8_t* data, uint16_t dataLength) {
	uint8_t buffer[dataLength + TX_FRAME + ALL_FRAME];
	uint16_t bufferLength;

	encodeTxMessage(data, dataLength, DIGI_BROADCAST_ADDR, buffer, &bufferLength, TransmitOption::DEFAULT);

	return _connection->sendMessage(buffer, bufferLength);
}

uint8_t wer::com::DigiRadioApi::unicast(uint8_t* data, uint16_t dataLength, uint16_t target) {
	uint64_t destinationAddr = DIGI_BROADCAST_ADDR;

	auto conv = _radioAdresses.find(target);
	if(conv != _radioAdresses.end()) {
		destinationAddr = conv->second;
	}

	uint8_t buffer[dataLength + TX_FRAME + ALL_FRAME];
	uint16_t bufferLength;

	encodeTxMessage(data, dataLength, destinationAddr, buffer, &bufferLength, TransmitOption::DEFAULT);

	return _connection->sendMessage(buffer, bufferLength);
}

void wer::com::DigiRadioApi::broadcastDiagnose(uint8_t* data, uint16_t dataLength) {
	uint8_t buffer[dataLength + TX_FRAME + ALL_FRAME];
	uint16_t bufferLength;

	for(auto it = _radioAdresses.begin(); it != _radioAdresses.end(); it++) {

			encodeTxMessage(data, dataLength, it->second, buffer, &bufferLength, 
							  TransmitOption::ACTIVE_TRACE_ROUTE | TransmitOption::DISABLE_ACK);

			_connection->sendMessage(buffer, bufferLength);
	}
}

void wer::com::DigiRadioApi::processIncomingData(uint8_t* data, uint16_t dataLength) {
	
	for(int i = 0; i < dataLength; i++) {
		
		if(decodeApiFrame(data[i])) {
			
			uint8_t* frameStart = _recvBuffer + DIGI_START_BYTE_FRAME;
			uint16_t frameLength = _recvBuffer[DIGI_START_BYTE_LENGTH] << 8 | _recvBuffer[DIGI_START_BYTE_LENGTH + 1];;
			
			//Byte 0 of the frame is the Frame ID
			switch((FrameId) frameStart[0]) {
				case FrameId::RX:
				{
					//TODO memory optimisation !!!
					uint8_t payload[1024];
					uint16_t payloadSize;
					uint64_t sourceAddress;
					uint8_t options;
					
					decodeRxMessage(frameStart, frameLength, sourceAddress, options, payload, payloadSize);
					
					uint16_t radioAddress = 0;
					for(auto address : _radioAdresses) {
						if(address.second == sourceAddress) {
							radioAddress = address.first;
						}
					}

					if(_recvCallback) {
						_recvCallback(payload, payloadSize, radioAddress);
					}
				}
				break;

				case FrameId::AT_RESPONSE:
				{
					//TODO Implement this feature
					/*
					uint8_t frameId, status;
					char cmd[3] = {0, 0, '\0'};
					char cmdResult[64];

					decode_at_response_message(frameStart, frameLength, frameId, cmd, cmdResult, status);

					if(strcmp(cmd, "DB") == 0) {
						FlightLog::getInstance()->logRadioStatus(static_cast<float>(-cmdResult[0]));
					}
					*/
				}
				break;

				case FrameId::TX_STATUS:
				{
					//TODO Implement this feature
					/*
					uint8_t frameId, retry, deliveryStatus, discoveryStatus;
					uint16_t destAddress;

					decode_tx_status_message(frameStart, frameId, destAddress, retry, deliveryStatus, discoveryStatus);

					if(deliveryStatus != DeliveryStatus::SUCCESS) {
						
					}
					*/
				}
				break;

				case FrameId::ROUTE_INFO:
				{
					//TODO Implement this feature
					/*
					uint8_t ackTimeoutCount, txBlockedCount;
					uint32_t timestamp;
					uint64_t destAddress, sourceAddress, respAddress, recvAddress;
					decode_route_information_message(frameStart, frameLength, timestamp, ackTimeoutCount, txBlockedCount, destAddress, sourceAddress, respAddress, recvAddress);

					LogRadio() << "Trace route info : tx(" << unsigned(txBlockedCount) << ") ack(" 
					<< unsigned(ackTimeoutCount) << "), src ["
					<< << std::hex << std::noshowbase << std::setw(16) << std::setfill('0')
					<< sourceAddress << "], dest [" 
					<< destAddress << "], resp [" 
					<< respAddress << "], recv [" 
					<< respAddress << "]";
					*/
				}
				break;
				//ADD trace route management
				//decode_route_information_message(uint8_t *frame, uint16_t frameLength, uint32_t &timestamp, uint8_t &ackTimeoutCount, uint8_t &txBlockedCount, uint64_t &destAddress, uint64_t &sourceAddress, uint64_t &respAddress, uint64_t &recvAddress)

				default:
					
				break;
			}
		}
		
	}
}

uint8_t wer::com::DigiRadioApi::decodeApiFrame(uint8_t inputChar) {
	static uint8_t decodeState = 0;
	static uint8_t decodeIndex = 0;
	static uint16_t packetLength;
	static uint8_t checksum;

	uint8_t returnCode = 0;

	

	switch(decodeState) {
		case 0:		//No packet start condition meet for now
		{
			if(inputChar == DIGI_START_DELIMITER) {
				_recvBuffer[0] = inputChar;

				decodeState++;
				decodeIndex = 1;
				checksum = 0;
			}
		}
		break;

		case 1:		//A start condition was meet, let's receive packet length
		{
			_recvBuffer[decodeIndex] = inputChar;
			decodeIndex++;
			
			if(decodeIndex == 3) {
				packetLength = _recvBuffer[DIGI_START_BYTE_LENGTH] << 8 | _recvBuffer[DIGI_START_BYTE_LENGTH + 1];
				decodeState++;

				if(packetLength + decodeIndex > DIGI_RECV_BUFFER_SIZE) {
					//The buffer is not big enough.... Let's reset the state machine and trhow an error
					decodeState = 0;
				}
			}
			else if(decodeIndex > 3) {
				//This is an error
			}
		}
		break;

		case 2:		//We have the packet length, let's receive the frame
		{
			_recvBuffer[decodeIndex] = inputChar;
			checksum += inputChar;
			decodeIndex++;

			if(decodeIndex == packetLength + DIGI_START_BYTE_FRAME) {
				decodeState++;
			}
			else if(decodeIndex > packetLength + DIGI_START_BYTE_FRAME) {
				//This is an error
			}
		}
		break;

		case 3:		//Frame received, we just received the checksum
		{
			_recvBuffer[decodeIndex] = inputChar;
			decodeIndex++;

			checksum = 0xFF - checksum;

			if(checksum == inputChar) {
				//The received data looks to be valid
				returnCode = 1;
			}

			decodeState = 0;
		}
		break;

		default:
			decodeState = 0;
		break;
	}
	

	return returnCode;
}

uint8_t wer::com::DigiRadioApi::computeChecksum(uint8_t *frame, uint16_t length) {
	uint8_t checksum = 0;
	
	for(uint16_t i = 0; i < length; i++) {
		checksum += frame[i];
	}

	return 0xFF - checksum;
}

void wer::com::DigiRadioApi::encodeApiHeader(uint8_t *buffer, uint16_t frameLength) {
	buffer[0] = DIGI_START_DELIMITER;
	buffer[1] = (uint8_t) ((frameLength & 0xFF00) >> 8);
	buffer[2] = (uint8_t) (frameLength & 0x00FF);
}

void wer::com::DigiRadioApi::encodeTxMessage(uint8_t *data, uint16_t dataLength, uint64_t destAddress, uint8_t *message, uint16_t *messageLength, TransmitOption transmitOption) {
	//Compute frame and message length
	uint16_t frameLength = dataLength + TX_FRAME;
	*messageLength = frameLength + ALL_FRAME;

	//Write the header
	encodeApiHeader(message, frameLength);

	//Write the frame
	message[3] = (uint8_t) FrameId::TX;

	if(destAddress == DIGI_BROADCAST_ADDR) {
		message[4] = 0; //Identifies the data frame for the host to correlate with a subsequent ACK.
	}
	else {
		message[4] = 1;
	}

	message[5] = (uint8_t) ((destAddress & 0xFF00000000000000) >> 56);
	message[6] = (uint8_t) ((destAddress & 0x00FF000000000000) >> 48);
	message[7] = (uint8_t) ((destAddress & 0x0000FF0000000000) >> 40);
	message[8] = (uint8_t) ((destAddress & 0x000000FF00000000) >> 32);
	message[9] = (uint8_t) ((destAddress & 0x00000000FF000000) >> 24);
	message[10] = (uint8_t) ((destAddress & 0x0000000000FF0000) >> 16);
	message[11] = (uint8_t) ((destAddress & 0x000000000000FF00) >> 8);
	message[12] = (uint8_t) destAddress & 0x00000000000000FF;
	message[13] = 0xFF; // Reserved
	message[14] = 0xFE; // Reserved
	message[15] = 0x00; //Broadcast radius
	message[16] = (uint8_t) transmitOption; //Transmit options
	memcpy(&message[17], data, dataLength);

	//Write the checksum
	message[*messageLength - 1] = computeChecksum(&message[3], frameLength);
}

void wer::com::DigiRadioApi::encodeAtCmdMessage(const char cmd[2], const char *cmdParam, uint8_t *message, uint16_t *messageLength) {
	uint16_t paramLength = strlen(cmdParam);
	uint16_t frameLength = paramLength + AT_CMD_FRAME;
	*messageLength = frameLength + ALL_FRAME;

	//Write the header
	encodeApiHeader(message, frameLength);

	message[3] = (uint8_t) FrameId::AT_CMD;
	message[4] = 1; // Frame ID
	message[5] = cmd[0];
	message[6] = cmd[1];

	strcpy((char*) &message[7], cmdParam);

	message[*messageLength - 1] = computeChecksum(&message[3], frameLength);
}

void wer::com::DigiRadioApi::encodeSendRssiCmdMessage(uint8_t *message, uint16_t *messageLength) {
	encodeAtCmdMessage("DB", "", message, messageLength);
}

void wer::com::DigiRadioApi::decodeTxStatusMessage(uint8_t *frame, uint8_t &frameId, uint16_t &destAddress, uint8_t &retry, uint8_t &deliveryStatus, uint8_t &discoveryStatus) {
	destAddress = frame[2] << 8 | frame[3];
	frameId = frame[1];
	retry = frame[4];
	deliveryStatus = frame[5];
	discoveryStatus = frame[6];
}

void wer::com::DigiRadioApi::decodeRxMessage(uint8_t *frame, uint16_t frameLength, uint64_t &sourceAddress, uint8_t &options, uint8_t *buffer, uint16_t &bufferSize) {
	sourceAddress = (uint64_t) frame[1] << 56 | (uint64_t) frame[2] << 48 | 
					(uint64_t) frame[3] << 40 | (uint64_t) frame[4] << 32 | 
					(uint64_t) frame[5] << 24 | (uint64_t) frame[6] << 16 | 
					(uint64_t) frame[7] << 8  | (uint64_t) frame[8];
	
	options = frame[11];

	bufferSize = frameLength - RX_RECEIVE;
	
	memcpy(buffer, &(frame[RX_RECEIVE]), bufferSize);
	
}

void wer::com::DigiRadioApi::decodeLegacyRxMessage(uint8_t *frame, uint16_t frameLength, uint64_t &sourceAddress, uint8_t &rssi, uint8_t &options, uint8_t *buffer, uint16_t &bufferSize) {
	sourceAddress = (uint64_t) frame[1] << 56 | (uint64_t) frame[2] << 48 | 
					(uint64_t) frame[3] << 40 | (uint64_t) frame[4] << 32 | 
					(uint64_t) frame[5] << 24 | (uint64_t) frame[6] << 16 | 
					(uint64_t) frame[7] << 8  | (uint64_t) frame[8];
	rssi = frame[9];
	options = frame[11];

	bufferSize = frameLength - RX_LEGACY_RECEIVE;
	memcpy(buffer, &(frame[RX_LEGACY_RECEIVE]), bufferSize);
}

void wer::com::DigiRadioApi::decodeAtResponseMessage(uint8_t *frame, uint16_t frameLength, uint8_t &frameId, char cmd[2], char *cmdResult, uint8_t &status) {
	frameId = frame[1];

	cmd[0] = frame[2];
	cmd[1] = frame[3];

	status = frame[4];

	uint16_t responseLength = frameLength - AT_RECEIVE_FRAME;
	memcpy(cmdResult, &(frame[AT_RECEIVE_FRAME]), responseLength);
	cmdResult[responseLength] = '\0';
}

void wer::com::DigiRadioApi::decodeRouteInformationMessage(uint8_t *frame, uint16_t frameLength, uint32_t &timestamp, uint8_t &ackTimeoutCount, uint8_t &txBlockedCount, uint64_t &destAddress, uint64_t &sourceAddress, uint64_t &respAddress, uint64_t &recvAddress) {
	// Source event : frame[1]
	// Length : frame[2]
	timestamp = (uint32_t) frame[3] << 24 | (uint32_t) frame[4] << 16 |
				(uint32_t) frame[5] << 8  | (uint32_t) frame[6];
	ackTimeoutCount = frame[7];
	txBlockedCount = frame[8];
	// Reserved : frame[9]
	destAddress = (uint64_t) frame[10] << 56 | (uint64_t) frame[11] << 48 | 
				  (uint64_t) frame[12] << 40 | (uint64_t) frame[13] << 32 | 
				  (uint64_t) frame[14] << 24 | (uint64_t) frame[15] << 16 | 
				  (uint64_t) frame[16] << 8  | (uint64_t) frame[17];
	sourceAddress = (uint64_t) frame[18] << 56 | (uint64_t) frame[19] << 48 | 
				  	(uint64_t) frame[20] << 40 | (uint64_t) frame[21] << 32 | 
				  	(uint64_t) frame[22] << 24 | (uint64_t) frame[23] << 16 | 
				  	(uint64_t) frame[24] << 8  | (uint64_t) frame[25];
	respAddress = (uint64_t) frame[26] << 56 | (uint64_t) frame[27] << 48 | 
				  (uint64_t) frame[28] << 40 | (uint64_t) frame[29] << 32 | 
				  (uint64_t) frame[30] << 24 | (uint64_t) frame[31] << 16 | 
				  (uint64_t) frame[32] << 8  | (uint64_t) frame[33];
	recvAddress = (uint64_t) frame[34] << 56 | (uint64_t) frame[35] << 48 | 
				  (uint64_t) frame[36] << 40 | (uint64_t) frame[37] << 32 | 
				  (uint64_t) frame[38] << 24 | (uint64_t) frame[39] << 16 | 
				  (uint64_t) frame[40] << 8  | (uint64_t) frame[41];
}