#pragma once

#include <string>
#include <functional>

namespace wer {
    namespace com {

		enum class Feature {
			GET_RSSI		= 0,
			SET_RECIPIENT	= 1,
			GET_SENDER		= 2,
			DO_TRACEROUTE	= 3,
			ACKNOWLEDGE		= 4
		};

		const uint8_t HIGHEST_PRIORITY = 5;

		class Link {
		public:
			Link(uint8_t priority, std::string name);
			virtual ~Link();

			virtual void start();

			void setReceivedCallback(std::function<void(uint8_t* data, uint16_t dataLength, uint16_t address)> recvCallback);

			uint8_t getPriority();
			std::string getName();

			bool isFuncSupport(Feature feature);
			//void registerFuncCallback(func, callback);

			//TODO Implement this for debugging and logging
			/*virtual string getErrorMessage(uint8_t errorId);*/

			virtual uint8_t broadcast(uint8_t* data, uint16_t dataLength) = 0;
			virtual uint8_t unicast(uint8_t* data, uint16_t dataLength, uint16_t adresses) = 0;
			virtual void broadcastDiagnose(uint8_t* data, uint16_t dataLength) = 0;

			//TODO same with link features
		protected:
				std::function<void(uint8_t*, uint16_t, uint16_t)> _recvCallback;

				std::string _linkName;
		private:
				uint8_t _priority;
		};
	}
}