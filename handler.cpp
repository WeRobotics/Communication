#include "handler.h"

#include "mavlink_channels.h"

#include <Library/logging/log.h>

wer::com::Handler::Handler() :
_timeoutHandler(nullptr),
_timerHandler(nullptr),
_system_id(0),
_component_id(0),
_mavType(MAV_TYPE_ONBOARD_CONTROLLER),
_pilotType(MAV_AUTOPILOT_GENERIC),
_missionId(0),
_onboardState(OnboardState::BOOTING),
_onboardMavState(MAV_STATE_BOOT),
_failsafeState(FailsafeAction::FORCE_WAIT),
_gsTimeoutActive(false),
_connectedStation(0),
_lastReceptionTime(std::chrono::steady_clock::now()),
_lastReceiveIsHighLatency(false),
_signatureSet(false),
_pingSequenceId(0)
{
	_highLatencyLink.link = nullptr;
	_highLatencyLink.receiver = nullptr;
}

wer::com::Handler::~Handler()
{
    stop();
    unregisterMavlinkMessageHandlers(this);

    std::lock_guard<std::mutex> lock(_interfacesMutex);
    for(auto interface : _interfaces) {
        if (interface.second.receiver) {
            uint8_t used_channel = interface.second.receiver->get_channel();
            delete(interface.second.receiver);
            delete(interface.second.link);

            MAVLinkChannels::Instance().checkin_used_channel(used_channel);
        }
    }

    _interfaces.clear();
}

void wer::com::Handler::init(std::shared_ptr<TimeoutHandler> timeoutHandler, std::shared_ptr<CallEveryHandler> timerHandler, uint8_t system_id, uint8_t component_id) {
    _timeoutHandler = timeoutHandler;
    _timerHandler = timerHandler;
    _system_id = system_id;
    _component_id = component_id;
}

void wer::com::Handler::start() {
    _timerHandler->add(std::bind(&wer::com::Handler::sendHeartbeat, this), 1.0f, &_heartbeatCookie);
}

void wer::com::Handler::stop() {
    _timerHandler->remove(_heartbeatCookie);
}

void wer::com::Handler::setSystemEventCallback(std::function<void(uint8_t, uint8_t, SystemEvent)> eventCallback) {
    _systemEventCallback = eventCallback;
}

void wer::com::Handler::setGroundStationStateChangedCallback(std::function<void(bool)> callback) {
    _groundStationStateChangedCb = callback;
	
	//Let's update it directly to ensure synchronisation !
	if(_groundStationStateChangedCb) {
		_groundStationStateChangedCb(_connectedStation > 0 ? true : false);
	}
}

void wer::com::Handler::setGroundStationStateChangedDirectCallback(std::function<void(bool)> callback) {
    _groundStationStateChangedDirectCb = callback;
	
	//Let's update it directly to ensure synchronisation !
	if(_groundStationStateChangedDirectCb) {
		_groundStationStateChangedDirectCb(_connectedStation > 0 ? true : false);
	}
}

void wer::com::Handler::setSignature(std::string signature) {
	if(signature.length() == 32) {
		memcpy(_signature, signature.c_str(), 32);
		_signatureSet = true;
		LogInfo() << "Signature for communication is set.";
	}
	else {
		LogErr() << "Could not set the signature. Length is " << signature.length() << " it should 32.";
	}
}

void wer::com::Handler::systemConnectionChanged(uint8_t systemId, uint8_t interfaceId, SystemType type, SystemEvent event) {
	//TODO count connected devices !
	_connectedGsMutex.lock();
	if(type == SystemType::SYSTEM_TYPE_STATION) {
		switch (event)
		{
		case SystemEvent::CONNECTION:
		case SystemEvent::RECONNECTION:
			_connectedStation++;
			break;

		case SystemEvent::DISCONNECTION:
			_connectedStation--;
			break;		
		
		default:
			//Should never happen...
			break;
		}

		LogInfo() << "Number of connected station changed : " << unsigned(_connectedStation);

		if(_connectedStation > 0) {
			if(_gsTimeoutActive) {
				LogInfo() << "New connected station stoping timeout.";
				_timeoutHandler->remove(_noGsTimeout_cookie);
				_gsTimeoutActive = false;
			}

			if(_groundStationStateChangedDirectCb) {
				_groundStationStateChangedDirectCb(true);
			}
			_groundStationStateChangedCb(true);
		}
		else {
			if(_gsTimeoutActive == false) {
				_gsTimeoutActive = true;
				LogInfo() << "No connected station starting timeout.";
				_timeoutHandler->add(std::bind(&Handler::groundStationDisconnected, this), 30.0f, &_noGsTimeout_cookie);
				if(_groundStationStateChangedDirectCb) {
					_groundStationStateChangedDirectCb(false);
				}
			}
		}
	}
	_connectedGsMutex.unlock();
	
	if(_systemEventCallback) {
		_systemEventCallback(interfaceId, systemId, event);
	}
}

void wer::com::Handler::groundStationDisconnected() {
	std::lock_guard<std::mutex> lock(_connectedGsMutex);
	_groundStationStateChangedCb(false);
	_gsTimeoutActive = false;
}

void wer::com::Handler::registerMavlinkMessageHandler(uint16_t msg_id,
                                                  mavlink_message_handler_t callback,
                                                  const void *cookie)
{
    std::lock_guard<std::mutex> lock(_mavlinkHandlerTableMutex);

    MAVLinkHandlerTableEntry entry = {msg_id, callback, cookie};
    _mavlinkHandlerTable.push_back(entry);
}

void wer::com::Handler::unregisterMavlinkMessageHandlers(const void *cookie)
{
    std::lock_guard<std::mutex> lock(_mavlinkHandlerTableMutex);

    for (auto it = _mavlinkHandlerTable.begin(); it != _mavlinkHandlerTable.end(); /* no ++it */) {
        if (it->cookie == cookie) {
            it = _mavlinkHandlerTable.erase(it);
        } else {
            ++it;
        }
    }
}

wer::hal::ConnectionResult wer::com::Handler::addLink(wer::com::Link* link) {
	return addLink(link, true);
}

wer::hal::ConnectionResult wer::com::Handler::addLink(wer::com::Link* link, bool signingRequired) {
    CommunicationReceiver interface;

    uint8_t channel;
    if (!MAVLinkChannels::Instance().checkout_free_channel(channel)) {
        return wer::hal::ConnectionResult::CONNECTIONS_EXHAUSTED;
    }

	if(link->getPriority() == 255 && _highLatencyLink.receiver) {
		return wer::hal::ConnectionResult::CONNECTIONS_EXHAUSTED;
	}

    interface.link = link;
	if(signingRequired && _signatureSet) {
		interface.receiver = new MAVLinkReceiver(channel, _signature);
	}
	else {
		interface.receiver = new MAVLinkReceiver(channel);
	}

	if(link->getPriority() == 255) {
		_highLatencyLink = interface;
		link->setReceivedCallback(std::bind(&wer::com::Handler::receiveMavlinkHighLatency, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, channel));
	}
	else {
		{
			std::lock_guard<std::mutex> lock(_interfacesMutex);
			_interfaces[channel] = interface;
		}
		link->setReceivedCallback(std::bind(&wer::com::Handler::receiveMavlink, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, channel));
	}


    

    return wer::hal::ConnectionResult::SUCCESS;
}

int32_t wer::com::Handler::getElapsedTimeFromLastMessage() {
	std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();

	std::lock_guard<std::mutex> _lock(_receptionTimeMutex);
	return std::chrono::duration_cast<std::chrono::milliseconds>(now - _lastReceptionTime).count();
}

void wer::com::Handler::receiveMavlinkHighLatency(uint8_t* data, uint16_t dataLength, uint16_t address, uint8_t interfaceId) {
	if(_highLatencyLink.receiver) {
    	_highLatencyLink.receiver->set_new_datagram((char*) data, dataLength);
        while (_highLatencyLink.receiver->parse_message()) {
			mavlink_message_t message = _highLatencyLink.receiver->get_last_message();
            receiveMessage(message, interfaceId);

			_lastReceiveIsHighLatency = true;
        }
	}
}

void wer::com::Handler::receiveMavlink(uint8_t* data, uint16_t dataLength, uint16_t address, uint8_t interfaceId) {
    auto interface = _interfaces.find(interfaceId);

    if(interface != _interfaces.end()) {

        interface->second.receiver->set_new_datagram((char*) data, dataLength);

        while (interface->second.receiver->parse_message()) {
			
			_receptionTimeMutex.lock();
            _lastReceptionTime = std::chrono::steady_clock::now();
			_receptionTimeMutex.unlock();

			mavlink_message_t message = interface->second.receiver->get_last_message();

            receiveMessage(message, interfaceId);

			_lastReceiveIsHighLatency = false;
        }
    }
}

void wer::com::Handler::receiveMessage(const mavlink_message_t &message, uint8_t interface)
{
	if(message.msgid == MAVLINK_MSG_ID_PING) {
		processPing(message, interface);
	}

    if(message.msgid == MAVLINK_MSG_ID_HEARTBEAT) {
        if(processHeartbeat(message, interface) == false) {
			mavlink_heartbeat_t heartbeat;
    		mavlink_msg_heartbeat_decode(&message, &heartbeat);

			SystemType sysType = SystemType::SYSTEM_TYPE_UNDEFINED;
			if(heartbeat.type == MAV_TYPE_GCS) {
				sysType = SystemType::SYSTEM_TYPE_STATION;
			}
			else if(heartbeat.type == MAV_TYPE_ANTENNA_TRACKER) {
				sysType = SystemType::SYSTEM_TYPE_ANTENNA; // WebServer !
			}

            System* newSystem =  new System(_timeoutHandler, message.sysid, sysType);
			newSystem->setEventCallback(std::bind(&Handler::systemConnectionChanged, this, message.sysid, std::placeholders::_1, sysType, std::placeholders::_2));
            
			newSystem->heartbeat(interface, 0);
            _systems.insert({message.sysid, newSystem});
        }
    }
    else {
        _mavlinkHandlerTableMutex.lock();

        for (auto it = _mavlinkHandlerTable.begin(); it != _mavlinkHandlerTable.end(); /* ++it */) {
            if (it->msg_id == message.msgid) {
                _mavlinkHandlerTableMutex.unlock();
                it->callback(message);
                _mavlinkHandlerTableMutex.lock();
            }

            ++it;
        }
        _mavlinkHandlerTableMutex.unlock();
    }
}

bool wer::com::Handler::processHeartbeat(const mavlink_message_t &message, uint8_t interface) {
    /* TODO :: WARNING THIS SHOULD ALL BE CONNECTION SPECIFIC !!! */
    mavlink_heartbeat_t heartbeat;
    mavlink_msg_heartbeat_decode(&message, &heartbeat);
    //TODO: check system state and store it

    auto system = _systems.find(message.sysid);
    if(system != _systems.end()) {
        system->second->heartbeat(interface, 0);
        return true;
    }

    return false;
}

uint8_t wer::com::Handler::getSystemStationSysId(SystemType type) {
    /*TODO*/
    return 1;
}

uint8_t wer::com::Handler::getSystemStationCompId(SystemType type) {
    /*TODO*/
    return 1;
}
uint8_t wer::com::Handler::getThisSysId() {
    return _system_id;
}

uint8_t wer::com::Handler::getThisCompId() {
    return _component_id;
}

void wer::com::Handler::setMavType(uint8_t mavType) {
    _mavType = mavType;
}

void wer::com::Handler::setPilotType(uint8_t pilotType) {
    _pilotType = pilotType;
}

void wer::com::Handler::setMissionId(uint8_t missionId) {
    _missionId = missionId;
}

void wer::com::Handler::setOnboardState(uint8_t onboardState) {
    _onboardState = onboardState;
}

void wer::com::Handler::setOnboardMavState(uint8_t onboardMavState) {
    _onboardMavState = onboardMavState;
}

void wer::com::Handler::setOnboardFailsafeState(uint8_t failsafeState) {
	_failsafeState = failsafeState;
}

void wer::com::Handler::setHighLatencyHeartbeatFields(mavlink_high_latency2_t* highLatencyMessage) {
	highLatencyMessage->type = _mavType;
	highLatencyMessage->autopilot = _pilotType;
	highLatencyMessage->custom0 = _missionId;
	highLatencyMessage->custom_mode = _onboardState;
	highLatencyMessage->custom1 = _onboardMavState;
}

bool wer::com::Handler::sendMessageHighLatency(const mavlink_message_t &message) {
    uint8_t data[MAVLINK_MAX_PACKET_LEN];
    uint16_t dataLength = mavlink_msg_to_send_buffer(data, &message);

	/*
	std::cout << "HL payload : 0x";
	for(int i = 0; i < dataLength; i++) {
		std::cout << ' ' << std::hex << unsigned(data[i]);
	}
	std::cout << std::endl;
	*/

	LogInfo() << "Trying to send high latency message now.";
	
	if(_highLatencyLink.link) {
		_highLatencyLink.link->broadcast(data, dataLength);
	}

    return true;
}

void wer::com::Handler::sendCommandAck(uint8_t state, uint16_t cmd, uint8_t sysId) {
	mavlink_command_ack_t ack;
	mavlink_message_t message;

	ack.result = state;
	ack.command = cmd;
	ack.target_system = sysId;

	mavlink_msg_command_ack_encode(getThisSysId(), getThisCompId(), &message, &ack);
	sendMessage(message);

	LogInfo() << "Ack sent to system " << unsigned(sysId) << " for  command " << unsigned(cmd) << " with status " << unsigned(state) << ".";
}

bool wer::com::Handler::sendMessage(const mavlink_message_t &message, uint8_t interface) {
	uint8_t data[MAVLINK_MAX_PACKET_LEN];
    uint16_t dataLength = mavlink_msg_to_send_buffer(data, &message);

	std::lock_guard<std::mutex> lock(_interfacesMutex);
	if(_interfaces.find(interface) != _interfaces.end()) {
		return _interfaces.find(interface)->second.link->broadcast(data, dataLength);
	}
	else {
		return false;
	}
}

bool wer::com::Handler::sendMessage(const mavlink_message_t &message) {
    /* TODO :: DEPENDING
        - BROADCAST : Send to all interfaces
        - UNICAST (Mavlink target system) : Select the good interface according to the station if the stations already exist, otherwise use a default intreface
    */

	if(message.msgid == MAVLINK_MSG_ID_COMMAND_ACK && (getElapsedTimeFromLastMessage() > 10000 || _lastReceiveIsHighLatency)) {
		return sendMessageHighLatency(message);
	}

    uint8_t data[MAVLINK_MAX_PACKET_LEN];
    uint16_t dataLength = mavlink_msg_to_send_buffer(data, &message);

    const mavlink_msg_entry_t *msg_entry = mavlink_get_msg_entry(message.msgid);

    std::lock_guard<std::mutex> lock(_interfacesMutex);

    if((msg_entry->flags & MAV_MSG_ENTRY_FLAG_HAVE_TARGET_SYSTEM) && reinterpret_cast<const uint8_t*>(message.payload64)[msg_entry->target_system_ofs]) {
        uint8_t targetSystem;
        targetSystem = reinterpret_cast<const uint8_t*>(message.payload64)[msg_entry->target_system_ofs];

        auto system = _systems.find(targetSystem);
        if(system != _systems.end() && system->second->connected()) {
            Link* link = nullptr;
            uint16_t address = 0;
            for(auto interface : _interfaces) {
                if( system->second->connected(interface.first) &&
                   (link == nullptr || interface.second.link->getPriority() < link->getPriority()) ) {
                    link = interface.second.link;
                    address = system->second->getAddress(interface.first);
                }
            }

            if(link->isFuncSupport(Feature::SET_RECIPIENT)) {
                link->unicast(data, dataLength, address);
            }
            else {
                link->broadcast(data, dataLength);
            }

            //TODO Use the preffered interface with a unicast tranmission
            //uint8_t unicast(uint8_t data, uint16_t dataLength, uint16_t adresses);
        }
        else {
            Link* link = nullptr;
            for(auto interface : _interfaces) {
                if(link == nullptr || interface.second.link->getPriority() <  link->getPriority()) {
                    link = interface.second.link;
                }
            }

            if(link) {
                link->broadcast(data, dataLength);
            }
        }

        //TODO Use prefered connection in broadcast
    }
    else {
        //Send over all channels in broadcast

        for(auto interface : _interfaces) {
            if(! interface.second.link->broadcast(data, dataLength)) {
                //The message failed to transmit -> Not sure we should actually stop to send on the others interfaces...
                //return false;
            }
        }
    }

    return true;
}

void wer::com::Handler::processPing(const mavlink_message_t &message, uint8_t interface) {
	mavlink_ping_t pingData;
	mavlink_msg_ping_decode(&message, &pingData);

	if(pingData.target_system == 0) {
		mavlink_ping_t pingBack;
		mavlink_message_t messageBack;

		pingBack.target_system = message.sysid;
		pingBack.target_component = message.compid;
		pingBack.seq = pingData.seq;
		pingBack.time_usec = pingData.time_usec;
		mavlink_msg_ping_encode(getThisSysId(), getThisCompId(), &messageBack, &pingBack);
		sendMessage(messageBack, interface);
	}
	else if(pingData.target_system == getThisSysId()) {
		uint64_t dt = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() - pingData.time_usec;
		LogMessage() << "PING " << unsigned(getThisSysId()) << " to " << unsigned(message.sysid) 
				  << " received: seq=" << pingData.seq << " int=" << unsigned(interface) << " time=" << dt << " ms";
	}
}


void wer::com::Handler::sendPing() {
	mavlink_ping_t pingData;
	mavlink_message_t message;

	pingData.target_system = 0;
	pingData.target_component = 0;
	pingData.seq = _pingSequenceId;
	pingData.time_usec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();

	_pingSequenceId++;

	mavlink_msg_ping_encode(getThisSysId(), getThisCompId(), &message, &pingData);
	sendMessage(message);
}


void wer::com::Handler::sendHeartbeat() {
    mavlink_message_t message;
    // GCSClient is not autopilot!; hence MAV_AUTOPILOT_INVALID.
	uint16_t state = _onboardState | (_failsafeState << 8);
    mavlink_msg_heartbeat_pack(getThisSysId(),
                               getThisCompId(),
                               &message,
                               _mavType,
                               _pilotType,
                               _missionId,
                               state,
                               _onboardMavState
                               );
    sendMessage(message);
}

/*
ConnectionResult MavlinkHandler::add_any_connection(const std::string &connection_url)
{
    CliArg cli_arg;
    if (!cli_arg.parse(connection_url)) {
        return ConnectionResult::CONNECTION_URL_INVALID;
    }

    switch (cli_arg.get_protocol()) {
        case CliArg::Protocol::UDP: {
            std::string path = DEFAULT_UDP_BIND_IP;
            int port = DEFAULT_UDP_PORT;
            if (!cli_arg.get_path().empty()) {
                path = cli_arg.get_path();
            }
            if (cli_arg.get_port()) {
                port = cli_arg.get_port();
            }
            return add_udp_connection(path, port);
        }

        case CliArg::Protocol::TCP: {
            std::string path = DEFAULT_TCP_REMOTE_IP;
            int port = DEFAULT_TCP_REMOTE_PORT;
            if (!cli_arg.get_path().empty()) {
                path = cli_arg.get_path();
            }
            if (cli_arg.get_port()) {
                port = cli_arg.get_port();
            }
            return add_tcp_connection(path, port);
        }
#ifndef ANDROID
        case CliArg::Protocol::SERIAL: {
            int baudrate = DEFAULT_SERIAL_BAUDRATE;
            if (cli_arg.get_baudrate()) {
                baudrate = cli_arg.get_baudrate();
            }
            return add_serial_connection(cli_arg.get_path(), baudrate);
        }

        case CliArg::Protocol::SPI: {
            int channel = DEFAULT_SPI_CHANNEL;
            if (cli_arg.get_channel()) {
                channel = cli_arg.get_channel();
            }
            return add_spi_connection(channel, DEFAULT_SPI_SPEED, DEFAULT_SPI_ATTN);
        }
#endif

        default:
            return ConnectionResult::CONNECTION_ERROR;
    }
}

ConnectionResult MavlinkHandler::add_udp_connection(const std::string &local_ip,
                                                      const int local_port)
{
    auto new_conn = std::make_shared<UdpConnection>(
        std::bind(&MavlinkHandler::receiveMessageInternal, this, std::placeholders::_1),
        local_ip,
        local_port);

    ConnectionResult ret = new_conn->start();
    if (ret == ConnectionResult::SUCCESS) {
        add_connection(new_conn);
    }
    return ret;
}

ConnectionResult MavlinkHandler::add_tcp_connection(const std::string &remote_ip, int remote_port)
{
    auto new_conn = std::make_shared<TcpConnection>(
        std::bind(&MavlinkHandler::receiveMessageInternal, this, std::placeholders::_1),
        remote_ip,
        remote_port);

    ConnectionResult ret = new_conn->start();
    if (ret == ConnectionResult::SUCCESS) {
        add_connection(new_conn);
    }
    return ret;
}

#ifndef ANDROID
ConnectionResult MavlinkHandler::add_serial_connection(const std::string &dev_path, int baudrate)
{
    auto new_conn = std::make_shared<SerialConnection>(
        std::bind(&MavlinkHandler::receiveMessageInternal, this, std::placeholders::_1),
        dev_path,
        baudrate);

    ConnectionResult ret = new_conn->start();
    if (ret == ConnectionResult::SUCCESS) {
        add_connection(new_conn);
    }
    return ret;
}

ConnectionResult MavlinkHandler::add_spi_connection(int channel, int speed, int attnGpio)
{
    auto new_conn = std::make_shared<SpiConnection>(
        std::bind(&MavlinkHandler::receiveMessageInternal, this, std::placeholders::_1),
        channel,
        speed,
        attnGpio);

    ConnectionResult ret = new_conn->start();
    if (ret == ConnectionResult::SUCCESS) {
        add_connection(new_conn);
    }
    return ret;
}
#endif

if(message.msgid == MAVLINK_MSG_ID_HEARTBEAT) {
    auto it = addrConversion.find(message.sysid);
    if(it == addrConversion.end()) {
        addrConversion[message.sysid] = sourceAddress;
        LogInfo() << "New device [" << unsigned(message.sysid) << "] : [0x" 
        << std::hex << std::noshowbase << std::setw(16) << std::setfill('0')
        << sourceAddress << "]";
    }
    else if(it->second != sourceAddress) {
        LogInfo() << "Conflict radio address [" << unsigned(message.sysid) << "] : [" 
        << std::hex << std::noshowbase << std::setw(16) << std::setfill('0')
        << sourceAddress << "] / [" 
        << it->second << "]";
    }
}

void MavlinkHandler::processStatustext(const mavlink_message_t &message)
{
    mavlink_statustext_t statustext;
    mavlink_msg_statustext_decode(&message, &statustext);

    std::string debug_str = "MAVLink: ";

    switch (statustext.severity) {
        case MAV_SEVERITY_EMERGENCY:
            debug_str += "emergency";
            break;
        case MAV_SEVERITY_ALERT:
            debug_str += "alert";
            break;
        case MAV_SEVERITY_CRITICAL:
            debug_str += "critical";
            break;
        case MAV_SEVERITY_ERROR:
            debug_str += "error";
            break;
        case MAV_SEVERITY_WARNING:
            debug_str += "warning";
            break;
        case MAV_SEVERITY_NOTICE:
            debug_str += "notice";
            break;
        case MAV_SEVERITY_INFO:
            debug_str += "info";
            break;
        case MAV_SEVERITY_DEBUG:
            debug_str += "debug";
            break;
        default:
            break;
    }

    // statustext.text is not null terminated, therefore we copy it first to
    // an array big enough that is zeroed.
    char text_with_null[sizeof(statustext.text) + 1]{};
    memcpy(text_with_null, statustext.text, sizeof(statustext.text));

    std::cout << debug_str << ": " << text_with_null;
}


bool MavlinkHandler::diagnose() {
    std::lock_guard<std::mutex> lock(_connections_mutex);

    for (auto it = _connections.begin(); it != _connections.end(); ++it) {
        if ((**it).hasDiagnose()) {
            if(!(**it).diagnose()) {
                LogInfo() << "Diagnose fail";
                return false;
            }
        }
    }

    return true;
}*/