#include "link.h"

wer::com::Link::Link(uint8_t priority, std::string name) :
_recvCallback(nullptr),
_linkName(name),
_priority(priority)
{

}

wer::com::Link::~Link() {

}

void wer::com::Link::start() {

}

uint8_t wer::com::Link::getPriority() {
	return _priority;
}

std::string wer::com::Link::getName() {
	return _linkName;
}

void wer::com::Link::setReceivedCallback(std::function<void(uint8_t*, uint16_t, uint16_t)> recvCallback) {
	_recvCallback = recvCallback;
}

bool wer::com::Link::isFuncSupport(wer::com::Feature feature) {
	return false;
}