#pragma once

#include <map>
#include <functional>

#include <Hardware/include/connection.h>

#include "link.h"

namespace wer {
    namespace com {

		class ConnectionLink : public Link {
		public:
			ConnectionLink(std::shared_ptr<wer::hal::Connection> connection, uint8_t priority, std::string name);
			~ConnectionLink();

			uint8_t broadcast(uint8_t* data, uint16_t dataLength);
			uint8_t unicast(uint8_t* data, uint16_t dataLength, uint16_t adresses);
			void broadcastDiagnose(uint8_t* data, uint16_t dataLength);

			void processIncomingData(uint8_t* data, uint16_t dataLength);

		private:

			std::shared_ptr<wer::hal::Connection> _connection;
		};
	}
}