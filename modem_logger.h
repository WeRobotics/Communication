#pragma once

#include <map>
#include <functional>
#include <chrono>

#include <Hardware/include/connection.h>
#include <Library/timer/call_every_handler.h>
#include <Library/timer/timeout_handler.h>

namespace wer {
    namespace com {

		class ModemLogger {
		public:
			ModemLogger(std::shared_ptr<wer::hal::Connection> connection, std::shared_ptr<TimeoutHandler> timeoutHandler, std::shared_ptr<CallEveryHandler> timerHandler);
			~ModemLogger();

			void start();
			void stop();

			struct CopsResponse {
				uint8_t mode;
				char network[30];
				uint8_t networkStrLength;
				uint8_t format;
				uint8_t act;
				std::chrono::steady_clock::time_point time;
				bool valid;
			};

			inline static const char * actToStr(uint8_t act) {
				switch (act)
				{
					case 0:
						return "GSM";
					case 2:
						return "UTRAN";
					case 3:
						return "GSM w/EGPRS";
					case 4:
						return "UTRAN w/HSDPA";
					case 5:
						return "UTRAN w/HSUPA";
					case 6:
						return "UTRAN w/HSDPA and HSUPA";
					case 7:
						return "E-UTRAN";
					
					default:
						return "Unknown";
						break;
				}
			}

			struct CsqResponse {
				uint8_t rssi;
				uint8_t ber;
				std::chrono::steady_clock::time_point time;
				bool valid;
			};

			struct BandResponse {
				uint8_t gsm;
				uint8_t hspa;
				uint8_t lte;
				std::chrono::steady_clock::time_point time;
				bool valid;
			};

			void decodeResponse(uint8_t* data, uint16_t dataLength);

			CopsResponse getCops();
			CsqResponse getCsq();
			BandResponse getBand();

		private:
			void sendNextAtCommand();
			void decodeCops(uint8_t data, uint8_t step);
			void decodeCsq(uint8_t data, uint8_t step);
			void decodeBand(uint8_t data, uint8_t step);
			void resetReception();

			static const int  RECV_BUF_SIZE = 10;

			std::shared_ptr<wer::hal::Connection> _connection;
			std::shared_ptr<TimeoutHandler> _timeoutHandler;
			std::shared_ptr<CallEveryHandler> _timerHandler;

			enum class ModemSendingState {
				WAITING_CSQ = 0,
				WAITING_COPS = 1,
			};

			uint8_t _cmdIndex;
			void* _sendAtCommandCookie;

			CopsResponse _tempCops;
			CopsResponse _actualCops;
			CsqResponse _tempCsq;
			CsqResponse _actualCsq;
			BandResponse _tempBand;
			BandResponse _actualBand;

			bool _isAnswered = false;
			uint8_t _decodeIndex = 0;
			uint8_t _decodeStep = 0;
			uint8_t _decodeCommand = 0;
			char _atRecvBuffer[RECV_BUF_SIZE];

			std::string _buffer;

			std::mutex _answerMutex;
		};
	}
}