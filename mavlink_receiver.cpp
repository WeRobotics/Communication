#include "mavlink_receiver.h"
#include <chrono>

static const uint32_t _unsigned_messages[] = {
	MAVLINK_MSG_ID_RADIO_STATUS,
	MAVLINK_MSG_ID_HIGH_LATENCY2,
	MAVLINK_MSG_ID_HEARTBEAT
};

mavlink_signing_streams_t wer::com::MAVLinkReceiver::_signing_streams;

static bool accept_unsigned_callback(const mavlink_status_t *status, uint32_t message_id) {
    /*
	// Make the assumption that channel 0 is USB and should always be accessible
    if (status == mavlink_get_channel_status(MAVLINK_COMM_0)) {
        return true;
    }
	*/

    for (unsigned i = 0; i < sizeof(_unsigned_messages) / sizeof(_unsigned_messages[0]); i++) {
        if (_unsigned_messages[i] == message_id) {
            return true;
        }
    }

    return false;
}

wer::com::MAVLinkReceiver::MAVLinkReceiver(uint8_t channel) :
    _channel(channel)
{}

wer::com::MAVLinkReceiver::MAVLinkReceiver(uint8_t channel, const char* key) :
    _channel(channel)
{	
	memset(&_signing, 0, sizeof(_signing));
	memcpy(_signing.secret_key, key, 32);
	_signing.link_id = channel;
	long long millisOffset = 142007040000;
	_signing.timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() / 10 - millisOffset;
	_signing.flags = MAVLINK_SIGNING_FLAG_SIGN_OUTGOING;
	_signing.accept_unsigned_callback = accept_unsigned_callback;

	mavlink_status_t *status = mavlink_get_channel_status(channel);
	status->signing = &_signing;
	status->signing_streams = &wer::com::MAVLinkReceiver::_signing_streams;
}

void wer::com::MAVLinkReceiver::set_new_datagram(char *datagram, unsigned datagram_len)
{
    _datagram = datagram;
    _datagram_len = datagram_len;
}

bool wer::com::MAVLinkReceiver::parse_message()
{
    // Note that one datagram can contain multiple mavlink messages.
    for (unsigned i = 0; i < _datagram_len; ++i) {
        if (mavlink_parse_char(_channel, _datagram[i], &_last_message, &_status) == 1) {
            // Move the pointer to the datagram forward by the amount parsed.
            _datagram += (i + 1);
            // And decrease the length, so we don't overshoot in the next round.
            _datagram_len -= (i + 1);

            // We have parsed one message, let's return so it can be handled.
            return true;
        }
    }

    // No (more) messages, let's give up.
    _datagram = nullptr;
    _datagram_len = 0;
    return false;
}

