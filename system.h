#pragma once

#include <unistd.h>

#include <chrono>
#include <iostream>
#include <functional>

#include <Library/timer/timeout_handler.h>

namespace wer {
    namespace com {

		enum class SystemType{SYSTEM_TYPE_UNDEFINED, SYSTEM_TYPE_ANTENNA, SYSTEM_TYPE_DRONE, SYSTEM_TYPE_STATION, SYSTEM_TYPE_RELAY, SYSTEM_TYPE_ADMIN};

		enum class SystemEvent{CONNECTION, RECONNECTION, DISCONNECTION};

		class System {
		public:
			System(std::shared_ptr<TimeoutHandler> timeoutHandler, uint8_t system_id, SystemType systemType);
			~System();

			void setEventCallback(std::function<void(uint8_t, SystemEvent)> eventCallback);

			//virtual void receiveMessage(const mavlink_message_t &message);
			SystemType getType();
			bool canOperate();
			void setOperate(bool canOperate);

			void heartbeat(uint8_t interface, uint16_t address);
			bool connected(uint8_t interface);
			bool connected();

			uint16_t getAddress(uint8_t interface);

		private:
			void heartbeatTimeout(uint8_t interface);

			std::shared_ptr<TimeoutHandler> _timeoutHandler;

			const uint8_t _systemId;
			const SystemType _systemType;

			bool _canOperate;

			struct ConnectionInfo {
				uint16_t address;
				bool connected;
				void* cookie;
			};

			//Pair : (Interface ID, Connection Info)
			std::map<uint8_t, ConnectionInfo> _connections;
			uint8_t _validConnections;

			std::function<void(uint8_t, SystemEvent)> _eventCallback;
		};
	}
}