#pragma once

#include <cstdint>

#include "mavlink_include.h"

namespace wer {
    namespace com {

        class MAVLinkReceiver {
        public:
            explicit MAVLinkReceiver(uint8_t channel);
			explicit MAVLinkReceiver(uint8_t channel, const char* key);

            uint8_t get_channel() { return _channel; }

            mavlink_message_t &get_last_message() { return _last_message; }

            mavlink_status_t &get_status() { return _status; }

            void set_new_datagram(char *datagram, unsigned datagram_len);

            bool parse_message();

			static mavlink_signing_streams_t _signing_streams;

        private:
            uint8_t _channel;
            mavlink_message_t _last_message = {};
            mavlink_status_t _status = {};
            char *_datagram = nullptr;
            unsigned _datagram_len = 0;
			mavlink_signing_t _signing;
        };
    }
}
