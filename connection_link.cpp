#include "connection_link.h"

#include <string.h>
#include <iomanip>

wer::com::ConnectionLink::ConnectionLink(std::shared_ptr<wer::hal::Connection> connection, uint8_t priority, std::string name) :
Link(priority, name),
_connection(connection)
{
	_connection->registerCallback(std::bind(&wer::com::ConnectionLink::processIncomingData, this, std::placeholders::_1, std::placeholders::_2));
}

wer::com::ConnectionLink::~ConnectionLink() {

}

uint8_t wer::com::ConnectionLink::broadcast(uint8_t* data, uint16_t dataLength) {
	return _connection->sendMessage(data, dataLength);
}

uint8_t wer::com::ConnectionLink::unicast(uint8_t* data, uint16_t dataLength, uint16_t target) {
	return broadcast(data, dataLength);
}

void wer::com::ConnectionLink::broadcastDiagnose(uint8_t* data, uint16_t dataLength) {
	broadcast(data, dataLength);
}

void wer::com::ConnectionLink::processIncomingData(uint8_t* data, uint16_t dataLength) {
	if(_recvCallback) {
		_recvCallback(data, dataLength, 0);
	}
}
