#pragma once

#include <map>
#include <functional>

#include <Hardware/include/connection.h>

#include "link.h"

#define DIGI_RECV_BUFFER_SIZE 1024

namespace wer {
    namespace com {

		enum class TransmitOption {
			DEFAULT					= 0x00,
			DISABLE_ACK 			= 0x01,
			DISABLE_ADDR_DISCOVERY	= 0x02,
			ACTIVE_TRACE_ROUTE		= 0x04,
			ACTIVE_NACK_ON_ROUTE	= 0x08
		};

		TransmitOption operator|(TransmitOption lhs, TransmitOption rhs);

		class DigiRadioApi : public Link {
		public:
			DigiRadioApi(std::shared_ptr<wer::hal::Connection> connection, uint8_t priority, std::string name);
			~DigiRadioApi();

			uint8_t broadcast(uint8_t* data, uint16_t dataLength);
			uint8_t unicast(uint8_t* data, uint16_t dataLength, uint16_t adresses);
			void broadcastDiagnose(uint8_t* data, uint16_t dataLength);

			void processIncomingData(uint8_t* data, uint16_t dataLength);

		private:

			uint8_t decodeApiFrame(uint8_t inputChar);

			void encodeApiHeader(uint8_t *buffer, uint16_t frameSize);
			void encodeTxMessage(uint8_t *data, uint16_t dataLength, uint64_t destAddress, uint8_t *message, uint16_t *messageLength, TransmitOption transmitOption);
			void encodeAtCmdMessage(const char cmd[2], const char *cmdParam, uint8_t *message, uint16_t *messageLength);
			void encodeSendRssiCmdMessage(uint8_t *message, uint16_t *messageLength);

			void decodeTxStatusMessage(uint8_t *frame, uint8_t &frameId, uint16_t &destAddress, uint8_t &retry, uint8_t &deliveryStatus, uint8_t &discoveryStatus);
			void decodeRxMessage(uint8_t *frame, uint16_t frameLength, uint64_t &sourceAddress, uint8_t &options, uint8_t *buffer, uint16_t &bufferSize);
			void decodeLegacyRxMessage(uint8_t *frame, uint16_t frameLength, uint64_t &sourceAddress, uint8_t &rssi, uint8_t &options, uint8_t *buffer, uint16_t &bufferSize);
			void decodeAtResponseMessage(uint8_t *frame, uint16_t frameLength, uint8_t &frameId, char cmd[2], char *cmdResult, uint8_t &status);
			void decodeRouteInformationMessage(uint8_t *frame, uint16_t frameLength, uint32_t &timestamp, uint8_t &ackTimeoutCount, uint8_t &txBlockedCount, uint64_t &destAddress, uint64_t &sourceAddress, uint64_t &respAddress, uint64_t &recvAddress);

			uint8_t computeChecksum(uint8_t *frame, uint16_t length);

			std::shared_ptr<wer::hal::Connection> _connection;
			uint8_t _recvBuffer[DIGI_RECV_BUFFER_SIZE];
			std::map<uint16_t, uint64_t> _radioAdresses; //mapping of specific radio adresses to a classic index
		};
	}
}