#pragma once

#include <cstdint>
#include <functional>
#include <atomic>
#include <vector>
#include <unordered_set>
#include <map>
#include <thread>
#include <mutex>
#include <future>
#include <string>

#include <Library/timer/call_every_handler.h>
#include <Library/timer/timeout_handler.h>
#include <Hardware/include/connection.h>
#include <Application/state.h>

#include "link.h"
#include "system.h"
#include "mavlink_include.h"
#include "mavlink_receiver.h"

/*
CHECK IS NEEDED !!!
    std::shared_ptr<std::atomic<OnboardState>> actualState
*/
namespace wer {
    namespace com {

        class Handler {
        public:

            static Handler* get() {
                static Handler instance;

                return &instance;
            }
            
            //TODO create a second constructor witch create it's own thread with it's own timer and timeout handler
            ~Handler();

            wer::hal::ConnectionResult addLink(Link* link);
			wer::hal::ConnectionResult addLink(Link* link, bool sign);

            void init(std::shared_ptr<TimeoutHandler> timeoutHandler, std::shared_ptr<CallEveryHandler> timerHandler, uint8_t system_id, uint8_t component_id);
            void start();
            void stop();

            void setSystemEventCallback(std::function<void(uint8_t, uint8_t, SystemEvent)> eventCallback);
			void setGroundStationStateChangedCallback(std::function<void(bool)> callback);
			void setGroundStationStateChangedDirectCallback(std::function<void(bool)> callback);

			int32_t getElapsedTimeFromLastMessage();
			void setHighLatencyHeartbeatFields(mavlink_high_latency2_t* highLatencyMessage);

            typedef std::function<void(const mavlink_message_t &)> mavlink_message_handler_t;

            void registerMavlinkMessageHandler(uint16_t msg_id, mavlink_message_handler_t callback, const void *cookie);
            //void registerMavlinkMessageHandler(uint16_t msg_id, mavlink_message_handler_t callback, SystemType type, const void *cookie);
            void unregisterMavlinkMessageHandlers(const void *cookie);

            //This function is virtual in case of redifinition on an extended class
            bool sendMessage(const mavlink_message_t &message);
			bool sendMessage(const mavlink_message_t &message, uint8_t interface);
			bool sendMessageHighLatency(const mavlink_message_t &message);
			void sendCommandAck(uint8_t state, uint16_t cmd, uint8_t sysId);

            //Incoming raw data to decode a mavlink packet
            void receiveMavlink(uint8_t* data, uint16_t dataLength, uint16_t address, uint8_t interface);
			void receiveMavlinkHighLatency(uint8_t* data, uint16_t dataLength, uint16_t address, uint8_t interfaceId);
            /* TODO think how to get additionnal information on the radio and exteran adresses*/

            //Return the ID of the system in control
            uint8_t getSystemStationSysId(SystemType type);
            uint8_t getSystemStationCompId(SystemType type);

            //Return the ID of this system
            uint8_t getThisSysId();
            uint8_t getThisCompId();

			//Mavlink sign packet
			void setSignature(std::string signature);

            void setMavType(uint8_t mavType);
            void setPilotType(uint8_t pilotType);
            void setMissionId(uint8_t missionId);
            void setOnboardState(uint8_t onboardState);
            void setOnboardMavState(uint8_t onboardMavState);
			void setOnboardFailsafeState(uint8_t failsafeState);

			void systemConnectionChanged(uint8_t interfaceId, uint8_t systemId, SystemType type, SystemEvent event);
			void groundStationDisconnected();

			void processPing(const mavlink_message_t &message, uint8_t interface);
			void sendPing();

        protected:
            std::shared_ptr<TimeoutHandler> _timeoutHandler;
            std::shared_ptr<CallEveryHandler> _timerHandler;


            std::mutex _systems_mutex;
            std::map<uint8_t, System*> _systems;

        private:
            Handler();
            
            // Stop the compiler generating methods of copy the object
            Handler(Handler const& copy);            // Not Implemented
            Handler& operator=(Handler const& copy); // Not Implemented

            void receiveMessage(const mavlink_message_t &message, uint8_t interface);

            bool processHeartbeat(const mavlink_message_t &message, uint8_t interface);

            void sendHeartbeat();

            void* _heartbeatCookie = nullptr;

            struct MAVLinkHandlerTableEntry {
                uint16_t msg_id;
                mavlink_message_handler_t callback;
                const void *cookie; // This is the identification to unregister.
            };

            uint8_t _system_id, _component_id;

            std::mutex _mavlinkHandlerTableMutex{};
            std::vector<MAVLinkHandlerTableEntry> _mavlinkHandlerTable{};

            struct CommunicationReceiver {
                Link* link;
                MAVLinkReceiver* receiver;
            };

            std::mutex _interfacesMutex;
            std::map<uint8_t, CommunicationReceiver> _interfaces;

            std::function<void(uint8_t, uint8_t, SystemEvent)> _systemEventCallback;
			std::function<void(bool)> _groundStationStateChangedCb;
			std::function<void(bool)> _groundStationStateChangedDirectCb;

            uint8_t _mavType;
            uint8_t _pilotType;
            uint8_t _missionId;
            uint8_t _onboardState;
            uint8_t _onboardMavState;
			uint8_t _failsafeState;

			std::mutex _connectedGsMutex;
			bool _gsTimeoutActive;
			int16_t _connectedStation;
			void* _noGsTimeout_cookie;

			std::mutex _receptionTimeMutex;
			std::chrono::time_point<std::chrono::steady_clock> _lastReceptionTime;

			CommunicationReceiver _highLatencyLink;
			bool _lastReceiveIsHighLatency;

			bool _signatureSet;
			char _signature[32];
			uint32_t _pingSequenceId;
        };
    }
}
/*
bool diagnose();
*/